# Contribution License Agreement

Yes, due to numerous legal issues, we have a [CLA](https://en.wikipedia.org/wiki/Contributor_License_Agreement). The current writer is not a lawyer,
so any actual legal professional who wants to make this mess actually binding is welcome. **This agreement will be enforced like it is binding, however.**

The goal of the CLA is to avoid legal troubles with OXO Industries, to comply with OXO Industries' weird, restrictive
[license agreement](https://github.com/OXOIndustries/TiTS-Public/blob/master/LICENSE.md), and to avoid drama. We may eventually
switch to a modified DCO, but for now, this is the best we can do.

**In order to contribute, you must agree to the following.**

1. Trials in Tainted Space ("TiTS"), and all content, trademarks, art, and code contained in the original version of the code are copyrights of OXO Industries.
2. Modular Code Overhaul ("MCO") is an open-source modification to Trials in Tainted Space, and is not currently endorsed, condoned, nor granted license to Trials in Tainted Space.
3. All contributions to MCO are not contributions to TiTS.
4. All contributions to MCO transfer all rights of the contribution to the collective "MCO Team", of which the creator may be a member. Contributors do not retain exclusive control over their creations, although credit will be given.
5. All contributions must comply with US federal law.
6. All contributions must be your own work, unless you have written permission.
7. Contributions may not contain any portion of code, art, or other asset from TiTS.
8. Contributions may be changed, rejected, or removed for any reason.

# Behaviour

Due to the source of our community, the following rules of conduct are required:

1. Don't be a dick.
2. Don't do things that will get MCO thrown off our host.
3. Don't do things that are illegal.

Failure to adhere to these rules will result in removal.

# Questions, comments

Questions and comments may be directed to Anonymous-BCFED:
* GitHub - Anonymous-BCFED
* GitLab - Anonymous-BCFED
* Discord - Anonymous-BCFED#8233
