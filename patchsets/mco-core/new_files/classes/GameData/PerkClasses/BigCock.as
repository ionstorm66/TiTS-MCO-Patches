﻿/* @NO AUTOGEN@
 *
 * Updated 04/14/19
 * Invoked:
 * Checked:
 */

package classes.GameData.PerkClasses {
	import classes.Creature;
	import classes.GameData.PerkData;
	import classes.StorageClass;
	import classes.Util.NumberValueHolder;

	public class BigCock extends PerkData {
		public function BigCock() {
			setID("Big Cock");
			perkName = "Big Cock";
			setAllNames(["Big Cock"]);
			perkDescription = "";
			setStorageValues(0, 0, 0, 0);
			
			setStorageName(1, "Multiplier");
		}
		
		public static const IDX_MULTIPLIER:int = 0;

		override public function Attach(c:Creature):void {
			super.Attach(c);
			c.OnCockIncrease.Attach(this, OnCockIncrease);
		}

		override public function Detach(c:Creature):void {
			super.Detach(c);
			c.OnCockIncrease.Detach(this, OnCockIncrease);
		}

		override public function Activate(c:Creature):void {
			super.Activate(c);
			// Called when added to a creature.
		}

		override public function Deactivate(c:Creature):void {
			super.Deactivate(c);
			// Called when removed from a creature.
		}

		override public function clone(sc:StorageClass):PerkData {
			var pd:BigCock = new BigCock();
			return _clone(sc, pd);
		}
		
		public function GetMultiplier(c:Creature):Number {
			return getValue(c, IDX_MULTIPLIER);
		}
		
		public function SetMultiplier(c:Creature, val:Number):void {
			setValue(c, IDX_MULTIPLIER, val);
		}

		// Automatically generated (definition at classes\Creature.as:8938)
		private function OnCockIncrease(c:Creature, increase:NumberValueHolder):void {
			if(DEBUG) trace(perkName+" - OnCockIncrease!")
			increase.value *= GetMultiplier(c);
		}

	}
}

