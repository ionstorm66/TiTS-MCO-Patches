﻿/* @NO AUTOGEN@
 *
 * Updated 04/14/19
 * Invoked:
 * - classes\Items\Miscellaneous\NukiCookies.as:463 (_parsedPerkCreationFindOnly): pc.createPerk("'Nuki Nuts",0,0,0,0,"Allows gonads to swell with excess seed.");
 * - classes\Items\Transformatives\NukiNutbutter.as:133 (_parsedPerkCreationFindOnly): target.createPerk("'Nuki Nuts", 0, 1, 0, 0, "Permanent ability allowing your gonads to swell with excess seed.");
 * - includes\creation.as:396 (_parsedPerkCreationFindOnly): pc.createPerk("'Nuki Nuts",0,0,0,0,"Allows gonads to swell with excess seed.");
 * Checked:
 * - classes\Items\Miscellaneous\NukiCookies.as:449: if(pc.balls >= 2 && pc.hasCock() && !pc.hasStatusEffect("Uniball") && changes < changeLimit && rand(2) == 0 && pc.ballSizeRaw >= 16 && pc.nukiScore() >= 3 && !pc.hasPerk("'Nuki Nuts"))
 * - classes\Items\Miscellaneous\ReductPro.as:948: if (pc.ballSizeRaw <= 4 && pc.hasPerk("'Nuki Nuts") && pc.race() != "kui-tan" && rand(10) == 0)
 * - classes\Items\Miscellaneous\ReductPro.as:965: if (pc.hasPerk("'Nuki Nuts"))
 * - classes\Items\Transformatives\NukiNutbutter.as:114: else if(!target.hasPerk("'Nuki Nuts"))
 * - classes\Items\Transformatives\SumaCream.as:166: if(pc.hasPerk("'Nuki Nuts") && pc.weightQ("testicle") >= 100 && pc.heightRatio("testicle") >= (40/60))
 * - classes\Items\Transformatives\SumaCream.as:356: if(!pc.hasPerk("'Nuki Nuts")) kGAMECLASS.output(" And unlike kui-tan testes, these don’t shrink back down.");
 * - includes\dynamicGrowth.as:258: if(pc.hasPerk("'Nuki Nuts")) msg += " If a quick fap wasn’t illegal here, this would be far simpler. Too bad.";
 * - includes\breedwell\breedwell.as:2008: if(pc.raceShort() == "kui-tan" || pc.hasPerk("'Nuki Nuts") || pc.isTreatedMale())
 * - includes\breedwell\breedwell.as:2016: else if(pc.hasPerk("'Nuki Nuts")) output(" kui-tan modders");
 * - includes\holidayEvents\freedomBeef.as:694: if (!pc.hasPerk("'Nuki Nuts")) output(" a needy kui-tan");
 * - includes\masturbation\bubbleBuddy.as:210: else if(pc.hasPerk("'Nuki Nuts") && pc.hasCock() && pc.balls > 0) addButton(3,"Drink It",drinkSomeBubbleBud,item,"Drink It","Your ‘nuki nuts could use a little booster. Bottoms up!");
 * - includes\newTexas\haley.as:1054: if (pc.hasPerk("'Nuki Nuts") == true) output(" It’s not like she could possibly know you have cum-inducing gene mods too.");
 * - includes\travelEvents\kiro.as:3726: if(pc.thinnestCockThickness() < 7 || (pc.hasPerk("'Nuki Nuts") && pc.hasStatusEffect("Blue Balls")))
 * - includes\travelEvents\shizuya.as:1133: if (pc.hasPerk("Nuki Nuts")) output(" Oh? I guess I’m pumping you up in more ways than one.”</i> Oh fuck, you’re getting so pent up that your stupid ‘nuki balls are starting to swell up! <i>“This is going to be a </i>lot<i> more fun than I thought.”</i>");
 * - includes\travelEvents\shizuya.as:1148: if (pc.hasPerk("Nuki Nuts")) output(", her hands darting down to press into your already massively inflated sack, sinking into it like a beanbag chair.");
 * - includes\vesperia\kally.as:3407: if(!pc.hasPerk("'Nuki Nuts")) output("... and I guess anybody with more nut than they know what to do with.");
 */

package classes.GameData.PerkClasses {
	import classes.Characters.PlayerCharacter;
	import classes.Creature;
	import classes.Engine.Interfaces.AddLogEvent;
	import classes.Engine.Interfaces.ExtendLogEvent;
	import classes.Engine.Interfaces.ParseText;
	import classes.Engine.Utility.rand;
	import classes.GLOBAL;
	import classes.GameData.PerkData;
	import classes.StorageClass;
	import classes.Util.BoolValueHolder;
	import classes.Util.NumberValueHolder;
	import classes.Util.StringValueHolder;
	import classes.kGAMECLASS;

	public class NukiNuts extends PerkData {
		public const VAL_BONUS:Number = 0;
		public const VAL_LOCK:Number = 1; // Unknown...?

		public function NukiNuts() {
			setID("Nuki Nuts");
			perkName = "'Nuki Nuts";
			setAllNames(["'Nuki Nuts"]);
			perkDescription = "Allows gonads to swell with excess seed.";
			setStorageValues(0, 0, 0, 0);
			setStorageName(0, "mLs of excess seed");
			setStorageName(1, "0 is natural (race-specific), 1 is not natural (permanent)");
		}

		override public function Attach(c:Creature):void {
			super.Attach(c);
			c.OnPenisOrgasm.Attach(this, OnPenisOrgasm);
			c.OnAfterCumQ.Attach(this, OnAfterCumQ);
			c.OnBallsFull.Attach(this, OnFilled);
			c.OnBallsOverfilled.Attach(this, OnOverfilling);
			c.OnPerkRacialUpdateCheck.Attach(this, OnPerkRacialUpdateCheck);
		}

		override public function Detach(c:Creature):void {
			super.Detach(c);
			c.OnPenisOrgasm.Detach(this, OnPenisOrgasm);
			c.OnAfterCumQ.Detach(this, OnAfterCumQ);
			c.OnBallsFull.Detach(this, OnFilled);
			c.OnBallsOverfilled.Detach(this, OnOverfilling);
			c.OnPerkRacialUpdateCheck.Detach(this, OnPerkRacialUpdateCheck);
		}

		override public function Activate(c:Creature):void {
			super.Activate(c);
			c.ballSizeRaw += 1.5;
			if(c.hasPerk("Bulgy")) c.ballSizeRaw += 1.5;
			c.orgasm();
			c.ballFullness = 100;
		}

		override public function Deactivate(c:Creature):void {
			super.Deactivate(c);
			kGAMECLASS.nutStatusCleanup();
		}

		override public function clone(sc:StorageClass):PerkData {
			var pd:NukiNuts = new NukiNuts();
			return _clone(sc, pd);
		}

		public function getBonus(c:Creature):Number {
			return getValue(c,VAL_BONUS);
		}
		public function setBonus(c:Creature,value:Number):void {
			setValue(c, VAL_BONUS,value);
		}

		public function isPermanent(c:Creature):Boolean {
			return getValue(c, VAL_LOCK) == 1;
		}
		public function setPermanent(c:Creature,on:Boolean):void {
			return setValue(c, VAL_LOCK, on ? 1 : 0);
		}

		// Automatically generated (definition at classes\Creature.as:3890)
		private function OnPenisOrgasm(pc:PlayerCharacter, msg:StringValueHolder, cumAmount:NumberValueHolder):void {
			if(DEBUG) trace(perkName+" - OnPenisOrgasm!")
			if(pc.balls <= 0) return;
			if(pc is PlayerCharacter)
			{
				msg.value = "Your";
				if(pc.balls == 1) msg.value += " testicle is back to its";
				else msg.value += " balls are back to their";
				msg.value += " normal size once more. What an incredible relief!";
				AddLogEvent(msg.value, "passive", -1);
			}
			pc.ballSizeMod -= getBonus(pc);
			setBonus(pc, 0);
		}

		// Automatically generated (definition at classes\Creature.as:11033)
		private function OnAfterCumQ(c:Creature, hQuantity:NumberValueHolder):void {
			if(DEBUG) trace(perkName+" - OnAfterCumQ!")
			//Overloaded nuki' nuts will fully drain
			if(c.balls > 0 && getBonus(c) > 0 && hQuantity.value < c.currentCum()) hQuantity.value = c.currentCum();
		}

		// Automatically generated (definition at classes\Creature.as:11145)
		private function OnFilled(c:Creature, m:StringValueHolder, cumCascade:Boolean):void {
			if(DEBUG) trace(perkName+" - OnFilled!")
			if(c.balls == 1)
			{
				if(cumCascade) m.value = ParseText("Your [pc.ballsNoun] has filled so much from your cum cascade that it’s started to swell. It won’t take much to excite you so long as your [pc.balls] is this full.");
				else m.value += " Of course, your kui-tan physiology will let your sack swell with additional seed. It’s already started to swell. Just make sure to empty it before it gets too big!";
			}
			else
			{
				if(cumCascade) m.value = ParseText("Your [pc.ballsNoun] have filled so much from your cum cascade that they’ve started to swell. It won’t take much to excite you so long as your [pc.balls] are this full.");
				else m.value += " Of course, your kui-tan physiology will let your balls balloon with additional seed. They’ve already started to swell. Just make sure to empty them before they get too big!";
			}
		}

		// Automatically generated (definition at classes\Creature.as:11180)
		private function OnOverfilling(c:Creature, capFullness:BoolValueHolder):void {
			if(DEBUG) trace(perkName+" - OnOverfilling!")
			if( c.balls > 0 && this is PlayerCharacter)
			{
				//Figure out a % of normal size to add based on %s.
				var nutChange:Number = (c.ballFullness/100) - 1;
				//Get the actual bonus number to add. Keep it to 2 decimals.
				var nutBonus:Number = Math.round(c.ballSizeRaw * nutChange * 100)/100;
				//FEN NOTE: I cut this from reductPro, but leaving the comment in case we change our mind about it.
				//if (c.hasStatusEffect("Slow Grow")) nutBonus = Math.round(nutBonus * statusEffectv1("Slow Grow") * 100) / 100;
				if (c.hasStatusEffect("No Grow")) nutBonus *= 0;
				trace("NUT BONUS: " + nutBonus);
				//Apply nutbonus and track in v1 of the perk
				c.ballSizeMod += nutBonus;
				setBonus(c, nutBonus + getBonus(c));
			}
		}

		// Automatically generated (definition at classes\Characters\PlayerCharacter.as:1389)
		private function OnPerkRacialUpdateCheck(pc:PlayerCharacter, deltaT:uint, doOut:Boolean):void {
			if(DEBUG) trace(perkName+" - OnPerkRacialUpdateCheck!")
			if(pc.balls <= 0 && getBonus(pc) != 0)
			{
				// Empty reserves!
				pc.ballSizeMod -= getBonus(pc);
				setBonus(pc, 0);
			}
			if(!isPermanent(pc) && pc.nukiScore() < 3)
			{
				if(pc.balls > 0)
				{
					//Nuts inflated:
					if(getBonus(pc) > 0)
					{
						AddLogEvent(ParseText("The extra size in your [pc.balls] bleeds off, making it easier to walk. You have a hunch that without all your"), "passive", deltaT);
						if(pc.originalRace.indexOf("kui-tan") != -1) ExtendLogEvent(" natural kui-tan genes");
						else ExtendLogEvent(" kui-tan body-mods");
						ExtendLogEvent(ParseText(", you won’t be swelling up with excess [pc.cumNoun] any more."));
					}
					//Nuts not inflated:
					else
					{
						AddLogEvent(ParseText("A tingle spreads through your [pc.balls]. Once it fades, you realize that your [pc.sack] is noticeably less elastic. Perhaps you’ve replaced too much kui-tan DNA to reap the full benefits."), "passive", deltaT);
					}
					ExtendLogEvent("\n\n(<b>Perk Lost: ‘Nuki Nuts</b>)");
					pc.ballSizeMod -= getBonus(pc);
					removeFrom(pc);
					kGAMECLASS.nutStatusCleanup();
				}
				else
				{
					AddLogEvent("(<b>Perk Lost: ‘Nuki Nuts</b> - You no longer meet the requirements. You’ve lost too many kui-tan transformations.)", "passive", deltaT);
					removeFrom(pc);
				}
			}
			else if(pc.balls <= 0) // PERK REWORK: Removed comment since it was breaking shit.
			{
				AddLogEvent("A strange sensation hits your nethers that forces you to wobble a little... Checking your status on your codex, it seems that removing your ballsack has also made the signature testicle-expanding tanuki mod vanish as well!\n\n(<b>Perk Lost: ‘Nuki Nuts</b> - You have no nuts to expand!)", "passive", deltaT);
				removeFrom(pc);
			}
		}

	}
}

